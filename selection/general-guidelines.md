---
description: The following are a few general guidelines to be followed.
---

# General Guidelines

Participants are required to make their repository private during the contest. On submission, participants must to change it back to public.

## **Plagiarism is strictly prohibited**

As stated above, referring online sources is allowed, but directly copying code from a website is prohibited. Sharing code between participants is also prohibited. Plagiarism checkers will be used to ensure all work done is genuine.

## **Google is your best friend**

Participants are allowed to refer to any online source \(tutorials, forums etc.\) for any doubts they may have. Helpful links will mostly be given along.

## **Reduce usage of GUI**

Basic tasks like navigation, editing files and unzipping should not be done using GUI i.e. the use of mouse and window managers is discouraged. Do as much of your work as possible on your terminal. ****

## **Disqualification**

 Participant\(s\) found violating any of the contest rules or displaying other unsporting behaviours will be subject to immediate disqualification by the organisers.

**You must not use Windows/similar for completing the tasks ,** use any Linux distribution/ macOS  

