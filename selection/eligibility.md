# Eligibility

**Requirements**

* You must be a current student of Amrita Vishwa Vidyapeetham, Amritapuri Campus.
* You must be ready to devote yourself to the club, and ready to devote your time for the club, such as staying back everyday and working on holidays.
* You must be willing to share and pass-on your knowledge with anyone, mentor your juniors, and do responsibilities and tasks assigned to you by the club.
* You must strictly adhere to the code of conduct of the club.

**Not Required**

* You need not be a CSE Student.
* You need not have any prior experience in Computer Science.

