# Contest Archive

The past tasks/contests held have been archived and put into seperate branches of this repository. If you are interested in joining the club, we strongly recommend that you checkout them, and practice them. Solution sets to the selected tasks/problems have also been put up.

| Title | Date | Type | Contestants |
| :--- | :--- | :--- | :--- |
| [Fresher's Contest - February 2019](feb-2019-freshers-challenge.md) | Feb 24, 2019 | Contest | S2 |
| [Fresher's Tasks - October 2018](october-2018-freshers-tasks.md) | October 2018 | Tasks | S1 |
| [Fresher's Tasks - July 2018](july-2018-freshers-tasks.md) | July 2018 | Tasks | S1 |



