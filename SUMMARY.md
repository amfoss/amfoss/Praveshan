# Table of contents

* [Praveshan - Join amFOSS](README.md)
* [About Club](https://amfoss.gitbook.io/amfoss/)

## Live Contests

* [Freshers Tasks 2019](live-contests/fresher-tasks.md)
* [Long Challenge](live-contests/long-challenge.md)

## Membership

* [Goals](https://amfoss.gitbook.io/amfoss/club/goals)
* [Benefits](https://amfoss.gitbook.io/amfoss/membership/benefits)
* [FAQ](https://amfoss.gitbook.io/amfoss/membership/frequently-asked-questions)

## Selection

* [Selection Process](selection/selection-process.md)
* [Eligibility](selection/eligibility.md)
* [General Guidelines](selection/general-guidelines.md)

## Formats

* [Long Challenge](formats/long-challenge.md)
* [Fresher's Contest](formats/freshers-contest.md)

## Contest Archive

* [Contest Archive](contest-archive/contest-archive/README.md)
  * [Feb 2019 - Fresher's Challenge](contest-archive/contest-archive/feb-2019-freshers-challenge.md)
  * [October 2018 - Fresher's Tasks](contest-archive/contest-archive/october-2018-freshers-tasks.md)
  * [July 2018 - Fresher's Tasks](contest-archive/contest-archive/july-2018-freshers-tasks.md)

