# Long Challenge

The Long Challenge primarily involves contributing to amFOSS projects.  Interested students can  contribute to the following projects in amFOSS  - 

### Web 

* [Qujini](https://github.com/amfoss/Qujini) 
* [GitLit](https://github.com/amfoss/GitLit)

### Android

* [TempleApp](https://github.com/amfoss/TempleApp)

A great starting point would be to have a look at issues with the label - "Good first bug" or "Junior" or similar tags.  



