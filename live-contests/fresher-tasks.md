# Freshers Tasks 2019

Freshers interested in joining the club by solving the tasks can do so - 

* Submit your details on this form - [https://forms.gle/cUSjcD3u7hSNAoio6](https://forms.gle/cUSjcD3u7hSNAoio6)
* Get the task document at the end of the form
* Submit the tasks before or on 11th August, 2019

You are expected to try the tasks only on Ubuntu or whatever distribution of Linux you have installed as part of the first task.

{% hint style="warning" %}
Deadline for submitting the tasks:   **11 August, 2019**

Please ****do not forget to check your mail regularly
{% endhint %}

{% hint style="danger" %}
**NOTE -  Form submissions are closed as of now . We are no longer accepting any submissions. Please reach to us at** amritapurifoss@gmail.com **if you have anything to say.**
{% endhint %}

## 



These tasks have been made keeping students having some background in programming in mind, but can be attempted by absolute beginners as well.

![](../.gitbook/assets/image%20%288%29.png)

