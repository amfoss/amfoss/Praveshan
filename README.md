---
description: >-
  Everything you need to know to join India's Leading FOSS & Computer Science
  Club.
---

# Praveshan - Join amFOSS

## How to Join FOSS@Amrita

![](.gitbook/assets/image.png)

If you are excited about joining FOSS@Amrita, there are multiple ways to gain membership - 

{% hint style="info" %}
For latest updates, please follow us on our [instagram\(@amfoss.in\)](https://instagram.com/amfoss.in) & [facebook\(@amfoss.in\)](https://facebook.com/amfoss.in) pages.
{% endhint %}

### \*\*\*\*

## **I. For Freshers \(S1/S2\)** 

### **Fresher's Tasks** 

Interested students are required to attempt to complete a list of given tasks. Evaluation will be done based on these tasks followed by an interview. ****

{% hint style="danger" %}
**Status : Closed.** 

**We are no longer accepting applications for tasks submission. Please reach out to us at** amritapurifoss@gmail.com**, if you have anything to say.**
{% endhint %}

{% hint style="warning" %}
**The registered people can submit the tasks before the deadline.**

Deadline : 11 August, 2019
{% endhint %}

## II. For Others \(S3 and above\) 

###      Long Challenge

Students can participate in the Long Challenge conducted by the club, get selected and join the club. For more details, refer[ Live Contests/Long Challenge.](https://join.amfoss.in/live-contests/freshers-workshop-2019)

![](.gitbook/assets/image%20%284%29.png)

